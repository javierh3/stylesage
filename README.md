# README #

### What is this repository for? ###

This is the second exercise of StyleSage's test.

### What the code supose to do? ###

The code takes an integer and return a string representation of that integer with commas separating groups of 3 digits.

### How do I get set up? ###

* You need python 3+
* You need pipenv
* You need a postgres server up and running

dump into your postgres the database attached to this repository called stylesage.dump

After this requirements has been satisfied just run:

```shell
pipenv install
```

After all dependencies has been installed in a virtualenv made for the app, run:

```shell
python main.py
```

### How do I try it? ###

* You can mount al the project into your local machine and follow the steps above. You can access the API in 127.0.0.1:5000
* You can access it in [this url](https://stylesage-test.herokuapp.com/). It's possible that when you try to request the first time it respond very slow or raise an error. This is because I'm using he free tier services of Heroku and they stops the machine when it has little use for some minutes.

* The endoints are these:
 - <https://stylesage-test.herokuapp.com/products/>
 - <https://stylesage-test.herokuapp.com/products/order_by_price/>
 - <https://stylesage-test.herokuapp.com/products/order_by_discount/>
 - <https://stylesage-test.herokuapp.com/products/top_discounted/>
 - <https://stylesage-test.herokuapp.com/products/of_color/COLOR/>
 