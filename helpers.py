from flask import jsonify

def make_response(data, pagination):
    page = pagination.get('page')
    per_page = pagination.get('per_page')

    data = data.paginate(page, per_page)

    return jsonify({
        'products': [d.serialize for d in data],
        'pagination': {
            'page': page,
            'per_page': per_page
        }
    })

    return response
