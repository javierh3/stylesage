from peewee import *
from playhouse.hybrid import *
from app import db

class BaseModel(Model):
    """A base model that will use our Postgresql database"""
    class Meta:
        database = db


class Product(BaseModel):
    title = CharField()
    original_price = DecimalField()
    current_price = DecimalField()
    color_name = CharField()

    @hybrid_property
    def discounted_amount(self):
        return self.original_price - self.current_price

    @property
    def serialize(self):
        return {
            'id': self.id,
            'title': self.title,
            'original_price': self.original_price,
            'current_price': self.current_price,
            'color_name': self.color_name,
            'discounted_amount': self.discounted_amount
        }
