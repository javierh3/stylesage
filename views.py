from app import app
from models import Product
from helpers import make_response
from flask import request

PER_PAGE = 20


@app.route('/products/', methods=['GET'])
def products():
    sort_by = request.args.get('sort_by', None)
    sort_direction = request.args.get('sort_direction', None)
    color_name = request.args.get('color', None)
    page = int(request.args.get('page', 1))

    products = Product.select()

    if color_name is not None:
        products = products.where(Product.color_name.contains(color_name))

    if sort_by and sort_by in ['current_price', 'discounted_amount']:
        sort_attribute = getattr(Product, sort_by)

        if sort_direction and sort_direction in ['asc', 'desc']:
            sort_attribute = getattr(sort_attribute, sort_direction)()

        products = products.order_by(sort_attribute)

    return make_response(products, {'page': page, 'per_page': PER_PAGE})


@app.route('/products/top_discounted/', methods=['GET'])
def top_discounted_products():
    products = Product.select().order_by(Product.discounted_amount.desc())

    return make_response(products, {'page': 1, 'per_page': PER_PAGE})


@app.route('/products/order_by_discount/', methods=['GET'])
def products_order_by_discount():
    page = int(request.args.get('page', 1))
    sort_direction = request.args.get('sort_direction', None)

    sort_method = Product.discounted_amount

    if sort_direction and sort_direction in ['asc', 'desc']:
        sort_method = getattr(sort_method, sort_direction)()

    products = Product.select().order_by(sort_method)

    return make_response(products, {'page': page, 'per_page': PER_PAGE})


@app.route('/products/order_by_price/', methods=['GET'])
def products_order_by_price():
    page = int(request.args.get('page', 1))
    sort_direction = request.args.get('sort_direction', None)

    sort_method = Product.current_price

    if sort_direction and sort_direction in ['asc', 'desc']:
        sort_method = getattr(sort_method, sort_direction)()

    products = Product.select().order_by(sort_method)

    return make_response(products, {'page': page, 'per_page': PER_PAGE})


@app.route('/products/of_color/<string:color>/', methods=['GET'])
def products_of_color(color=''):
    page = int(request.args.get('page', 1))

    products = Product.select().where(Product.color_name.contains(color))

    return make_response(products, {'page': page, 'per_page': PER_PAGE})
