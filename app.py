import os

from flask import Flask
from peewee import *
from playhouse.db_url import connect


app = Flask(__name__)

if 'HEROKU' in os.environ:
    db = connect(os.environ.get('DATABASE_URL'))
else:
    db = PostgresqlDatabase('stylesage', user='tesla')
